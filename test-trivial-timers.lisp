;;; -*- Mode: LISP; Package: SUPERVISOR; Syntax: Common-lisp;  -*-
;;;; test-trivial-timers.lisp

(in-package #:test-trivial-timers)

;;; "test-trivial-timers" goes here. Hacks and glory await!

(defparameter *first-thread-object* nil)
(defparameter *second-thread-object* nil)
(defparameter *first-parameter* nil)
(defparameter *second-parameter* nil)
(defparameter *state* :idle)
(defparameter *start-time* nil)
(defparameter *end-time* nil)
(defparameter *time-interval* 15.0)


(defun timeout-handler ()
  (setq *end-time* (local-time:now))
  (format *standard-output* "Delta t: ~a s" (local-time:timestamp-difference *end-time* *start-time*))
  (setq *state* :exit))

(defun start-test ()
  (setq *start-time* (local-time:now))
  (setq *first-thread* (bordeaux-threads:make-thread #'(lambda ()
							 (unwind-protect
							      (progn
								(setq *first-timer* (trivial-timers:make-timer #'timeout-handler
													       :name "timeout-timer"
													       :thread t))
								(trivial-timers:schedule-timer *first-timer* *time-interval*)
								(loop named thread-loop
								   when (eql *state* :exit) do
								     (return-from thread-loop))))))))
