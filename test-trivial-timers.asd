;;; -*- Mode: LISP; Package: SUPERVISOR; Syntax: Common-lisp;  -*-
;;;; test-trivial-timers.asd

(asdf:defsystem #:test-trivial-timers
  :description "Describe test-trivial-timers here"
  :author "Your Name <your.name@example.com>"
  :license "Specify license here"
  :depends-on (#:bordeaux-threads
               #:trivial-timers
	       #:local-time
	       #:time-interval)
  :serial t
  :components ((:file "package")
               (:file "test-trivial-timers")))

